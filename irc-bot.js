module.exports = (nick, password, channel, ircAddress, port, webAddress, webPort, db) => {
    const IRC = require('irc-framework')
    const nickservMiddleware = require('./irc-bot-middleware')

    const bot = new IRC.Client();
    bot.connect({
        host: ircAddress,
        port: port,
        nick: nick
    });

    const saveMessage = (sender, target, message) => {
	console.log('SQL insert message:', sender, target, message);
	db.serialize(function() {
            var stmt = db.prepare("INSERT INTO logs(nick, target, message) VALUES (?, ?, ?)");
            stmt.run(sender, target, message);
            stmt.finalize();
        });
    }

    bot.on('message', function(event) {
        saveMessage(event.nick, event.target, event.message);
    });

    bot.on('topic', function(event) {
	if( typeof event.nick !== 'undefined' ) {
	    saveMessage('Topic changed by ' + event.nick, event.channel, event.topic);
	}	
	else
	    saveMessage('Topic', event.channel, event.topic);
    });

    bot.matchMessage(/^hi/, function(event) {
        event.reply('hej!');
	saveMessage(nick, event.target, 'hej!');
    });

    bot.matchMessage(/^oya/, function(event) {
        event.reply('do zobaczenia!');
	saveMessage(nick, event.target, 'do zobaczenia!');
    })

    bot.matchMessage(/^dobranoc/, function(event) {
        event.reply('dobrych snów!');
	saveMessage(nick, event.target, 'dobrych sn�w!');
    });

    bot.matchMessage(/^logi/, function(event) {
	let reply = 'logi znajdziesz na: http://'+webAddress+':'+webPort+'/logs';
	console.log(reply);
        event.reply(reply);
	saveMessage(nick, event.target, reply);
    });

    bot.use(nickservMiddleware(nick, password, channel));
}

