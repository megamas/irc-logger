if(process.argv.length !== 10)
{
    console.log("Example usage: node irc-app.js web_address web_port database_path irc_address irc_port irc_channel nickname nickserv_password");
    process.exit(0);
}

const webAddress = process.argv[2];
const webPort = parseInt(process.argv[3]);
const dbPath = process.argv[4];

const ircAddress = process.argv[5];
const ircPort = parseInt(process.argv[6]);
const ircChannel = process.argv[7];
const ircNickname = process.argv[8];
const ircNickservPassword = process.argv[9];

let error = false;
if(isNaN(webPort) || webPort < 0) {
    console.log("The specified web port is not a positive integer value");
    error = true;
}

if(isNaN(ircPort) || ircPort < 0) {
    console.log("The specified irc port is not a positive integer value");
    error = true;
}
if(error)
    process.exit(0)

const express = require('express');
const bodyParser = require('body-parser');
const logger = require('morgan');
const path = require('path');

const sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database(dbPath);

db.serialize(function() {
    db.run(`CREATE TABLE IF NOT EXISTS logs (
        id integer PRIMARY KEY,
        date DATETIME DEFAULT (datetime('now','localtime')),
        nick text NOT NULL,
        target text,
        message text NOT NULL);`
    )
});

const router = require('./router.js')(db, webAddress, webPort, ircChannel);
const ircBot = require('./irc-bot.js');
ircBot(ircNickname, ircNickservPassword, ircChannel, ircAddress, ircPort, webAddress, webPort, db)

const app = express();
app.set('view engine', 'pug')
app.set('views', path.join(__dirname, 'views'));
app.use(logger('dev'));
app.use(bodyParser.urlencoded({extended: true}));
app.use('/logs', router);

app.use(function(req, res, next) {
    res.send('');
});
app.listen(webPort, () => {
    console.log('Server is running on port ' + webPort);
})