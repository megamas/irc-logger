module.exports = (db, webAddress, webPort, channel) => {
    //const locdb = db;    

    const express = require('express');
    const router = express.Router();
    
    var isPositiveInteger = (str) => {
	return /^\+?[1-9][\d]*$/.test(str);
    }

    var getMessagesPromise = (argNum) => {
	return new Promise(resolve => {
	    const offset = argNum * 100;
	    let sql = `SELECT date, nick, message FROM logs WHERE target='${channel}' ORDER BY id DESC LIMIT 100 OFFSET ${offset};`;
 	    let messages = [];
	    db.all(sql, [], (err, rows) => {
		if (err) {
		    console.log("@")
		    resolve([]);
		}
		rows.forEach((row) => {
		    messages.push({date: row.date, author: row.nick+':', text: row.message});
		});
		resolve(messages)
	    });	
	})
    };

    var getMessagesObj = async (argNum) => {
	var result = { 
		prevID: argNum - 1,
		nextID: argNum + 1,
		prevWebHref: 'http://' + webAddress + ':' + webPort + '/logs/' + (argNum - 1), 
		nextWebHref: 'http://' + webAddress + ':' + webPort + '/logs/' + (argNum + 1),
		messages: []
	};
    
	var dbMessagesPromise = await getMessagesPromise(argNum);
	result.messages = dbMessagesPromise;
	return result;
    }

    var getMessages = (arg) => {
	if(!isPositiveInteger(arg))
	    return getMessagesObj(0);
	
	return getMessagesObj(parseInt(arg));
    }
   
    router.get('/:currNum', (req, res) => {
	//console.log(req.params)
	getMessages(req.params.currNum).then((result) => {
	    res.render('index', result)
	});
    });

    router.get('/*', (req, res) => {
	getMessages(0).then((result) => {
	    res.render('index', result)
	})
    });

    return router;
};