//TODO: array of channel names
module.exports = (nick, password, channel) => {
    return function(client, raw_events, parsed_events) {
        parsed_events.use(theMiddleware);
    }

    function theMiddleware(command, event, client, next) {
        if (command === 'registered') {
            client.say('nickserv', 'identify ' + nick + ' ' + password);
        }

        if (command === 'message' && event.nick.toLowerCase() === 'nickserv') {
            console.log(event.message);
            client.join(channel);
        }

        next();
    }
}